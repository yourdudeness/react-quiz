import React, { useState } from 'react';

import classes from './Quiz.module.scss';
import ActiveQuiz from '../../components/ActiveQuiz/ActiveQuiz';
import FinishedQuiz from '../../components/FinishedQuiz/FinishedQuiz';

function Quiz() {
  const [quiz, setQuiz] = useState([
    {
      question: 'Текст вопроса',
      rightAnswerId: 2,
      id: 1,
      answers: [
        {
          text: 'Вопрос 1',
          id: 1,
        },
        {
          text: 'Вопрос 2',
          id: 2,
        },
        {
          text: 'Вопрос 3',
          id: 3,
        },
        {
          text: 'Вопрос 4',
          id: 4,
        },
      ],
    },

    {
      question: 'Текст вопроса2',
      rightAnswerId: 3,
      id: 2,
      answers: [
        {
          text: 'Вопрос 1',
          id: 1,
        },
        {
          text: 'Вопрос 2',
          id: 2,
        },
        {
          text: 'Вопрос 3',
          id: 3,
        },
        {
          text: 'Вопрос 4',
          id: 4,
        },
      ],
    },
  ]);

  const [results, setResults] = useState({});

  const [activeQuestion, setActiveQuestion] = useState(0); // с помощью данного каунтера идем по списку всех вопросов
  const [answerState, setAnswerState] = useState(null); //Информация о текущем клике пользователя(здесь мб либо правильный либо не правильный ответ){[id]: 'success' 'error'}

  const [isFinished, setIsFinished] = useState(false); //Изменяется когда опрос пройден

  function onAnswerClickHandler(answerId) {
    if (answerState) {
      //если сейчас есть какой то стейт, если здесь сейчас находится правильный ответ, то тогда не должны выполнять данную функцию
      const key = Object.keys(answerState)[0]; //вытаскиваем состояние данного ключи и забираем из него 0 элмент
      if (answerState[key] === 'success') {
        // если
        return;
      }
    }

    const questions = quiz[activeQuestion]; // получаем доступ к объекту answers в объекте quiz
    const result = results;

    if (questions.rightAnswerId === answerId) {
      if (!result[questions.id]) {
        result[questions.id] = 'success';
      }

      setResults({
        result,
      });

      setAnswerState(
        {
          [answerId]: 'success',
        }, //ключ answerId по которому мы кликнули
      );

      const timeout = window.setTimeout(() => {
        if (isQuizFinished()) {
          setIsFinished(true);
        } else {
          setActiveQuestion((prev) => prev + 1);
          setAnswerState(null); // обнуление состояния стейта если правильно ответили на вопрос
        }
        window.clearTimeout(timeout); // убираем таймаут как только закончится эта функция
      }, 1000);
    } else {
      result[questions.id] = 'error';
      setAnswerState({
        [answerId]: 'error',
      });

      setResults({
        result,
      });
    }
  }

  function isQuizFinished() {
    return activeQuestion + 1 === quiz.length; //если activeQuestion равняется длинне всего массива, то голосвание закончено и нужно делать какую то логику
  }

  function onRetryHandler() {
    setIsFinished(false);
    setActiveQuestion(0);
    setAnswerState(null);
  }

  return (
    <div className={classes.Quiz}>
      <div className={classes.QuizWrapper}>
        <h1>Ответьте на все вопросы </h1> {/* {QuestionList} */}
        {isFinished ? (
          <FinishedQuiz onRetry={onRetryHandler} results={results} quiz={quiz} />
        ) : (
          <ActiveQuiz
            question={quiz[activeQuestion].question}
            answers={quiz[activeQuestion].answers}
            onAnswerClick={onAnswerClickHandler}
            quizLength={quiz.length}
            answerNumber={activeQuestion + 1}
            state={answerState}
          />
        )}
      </div>{' '}
    </div>
  );
}

export default Quiz;
