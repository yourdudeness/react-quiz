import React from 'react'
import classes from './AnswerItem.module.scss'

function AnswerItem(props) {
    let cls = [classes.AnswersItem]

    if(props.state){ // если props.state не равняется 0, то данное состояние характрено только для данного айтема тут будем хранится либо succes либо error
        cls.push(classes[props.state])
    }

    return (
        <li
            className={cls.join(' ')}
            onClick={() => props.onAnswerClick(props.answer.id)}
        >
            { props.answer.text}
        </li>
    )
}

export default AnswerItem;