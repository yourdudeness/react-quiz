import React from 'react'
import classes from './AnswerList.module.scss'

import AnswerItem from './AnswerItem/AnswerItem'


function AnswerList(props) {
    return (
        <ul className={classes.AnswersList}>
            {props.answers.map((answer, index) => {
                
                return (
                    <AnswerItem
                        key={index}
                        answer={answer}
                        onAnswerClick= {props.onAnswerClick}
                        state={props.state ? props.state[answer.id] : null} //если есть props.state(потому что по умолчанию он null), то тогда передаем props.state по answer.id а если этого объекта нет то передаем null
                    />
                )
            })}

        </ul>
    )
}

export default AnswerList